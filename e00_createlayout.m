e_setup

file_chanlocs = fullfile(rootdir,'code/chanlocs_ced.txt');

if bids.validate(dirRaw)
    error('Raw data is messed up!')
end
disp('Ready to go...')

%%

elec = [];
tmp = readtable(file_chanlocs, 'FileType','text', 'Delimiter', '\t');
elec.chanpos = [tmp.X tmp.Y tmp.Z];
elec.chanpos = elec.chanpos(:, [2 1 3]);
elec.elecpos = elec.chanpos;
elec.chantype = repmat({'eeg'},72,1);
elec.chantype(71:72) = {'eog','eog'}';
elec.chanunit = repmat({'V'},72,1);
elec.label = tmp.labels;
elec.type = 'eeg1020';
elec.unit = 'mm';


fig(12,1);clf
subplot(121);
scatter3(elec.elecpos(:,1),elec.elecpos(:,2),elec.elecpos(:,3))
title('Original locations')
axis equal
%%
cfg = [];
% cfg.elec = elec;
% cfg.projection = 'polar';
cfg.layout = 'biosemi64.lay';
lay = ft_prepare_layout(cfg);
lay.pos(65:72,:) = [-.2 0.45
    .2 0.45
    -.25 0.5
    .25 0.5
    -0.5   -0.1
    0.5   -0.1
    NaN NaN
    NaN NaN
    ];
lay.width(67:72,:) = lay.width(1,:);
lay.height(67:72,:) = lay.height(1,:);
lay.label(65:72) = elec.label(65:72);


lay.pos = lay.pos - lay.pos(chnb('Cz',lay.label),:);
clear tmp
lay.pos(:,2) = lay.pos(:,2) * 1.1;
[tmp(:,1), tmp(:,2)] = cart2pol(lay.pos(:,1),lay.pos(:,2));
tmp(:,2) = tmp(:,2) * 1.25;
[lay.pos(:,1), lay.pos(:,2)] = pol2cart(tmp(:,1),tmp(:,2));


subplot(122)
cla;
ft_plot_layout(lay, 'box','no');
title('Customized layout')

save("mylayout.mat","lay")

% %%
% cfg = [];
% % cfg.elec = elec;
% % cfg.projection = 'polar';
% cfg.layout = 'actiCAP128_lay.mat';
% 
% lay = ft_prepare_layout(cfg);
% ft_plot_layout(lay, 'box','no');
% title('Customized layout')

%%

